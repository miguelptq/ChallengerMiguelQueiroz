<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/*$router->get(
    '/',
    function () use ($router) {
    }
);*/

/*$router->group(['prefix'=>'api/v1'], function() use($router){
        //return "Hello from swk dev team";
        $router->get('/videos', 'VideoController@ListarVideos');
        $router->post('/videos', 'VideoController@InserirVideos');
        //$router->get('/video/{id}', 'VideoController@show');
        $router->put('/video/{id}', 'VideoController@updateVideo');
        $router->delete('/video/{id}', 'VideoController@destroyVideo');
    }
);*/

$router->post('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

  $router->get('/videos',  ['uses' => 'VideoController@ListarVideos']);

  //$router->get('authors/{id}', ['uses' => 'AuthorController@showOneAuthor']);

  $router->post('/videos', ['uses' => 'VideoController@InserirVideos']);

  $router->delete('/video/{id}', ['uses' => 'VideoController@destroyVideo']);

  $router->put('/video/{id}', ['uses' => 'VideoController@updateVideo']);


});