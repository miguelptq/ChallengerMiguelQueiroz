<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','url_chanel', 'title', 'description', 'author', 'datetime'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
