<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;


class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function ListarVideos()
    {
     
     $videos = Video::all();

     return response()->json($videos);
     //return response()->json(Video::all());

    }

     public function InserirVideos(Request $request)
     {
        /*$this->validate($request, [
            'title' => 'required',
            'url_chanel' => 'required',
            'description' => 'required',
            'author' => 'required',
            'datetime' => 'required'
        ]);*/
        /*$video = new Video;

       $video->title= $request->title;
       $video->url = $request->url;
       $video->description= $request->description;
       $video->author= $request->author;
       $video->datetime= $request->datetime;
       
       $video->save();

       return response()->json($video);*/
       /*$video = Video::create($request->all());

        return response()->json($video, 201);*/


        function getYouTubeXMLUrl( $url, $return_id_only = false ) {

         $xml_youtube_url_base = 'https://www.youtube.com/feeds/videos.xml';
         $preg_entities        = [
             'channel_id'  => '\/channel\/(([^\/])+?)$', //match YouTube channel ID from url
             'user'        => '\/user\/(([^\/])+?)$', //match YouTube user from url
             'playlist_id' => '\/playlist\?list=(([^\/])+?)$',  //match YouTube playlist ID from url
         ];
         foreach ( $preg_entities as $key => $preg_entity ) {
            if ( preg_match( '/' . $preg_entity . '/', $url, $matches ) ) {
                if ( isset( $matches[1] ) ) {
                    if($return_id_only === false){
                        return $xml_youtube_url_base . '?' . $key . '=' . $matches[1];
                    }else{
                        return [
                            'type' => $key,
                            'id' => $matches[1],
                        ];
                    }
    
                }
            }
        }
    
    }
    
    $url = $request->url_chanel;
    $xml_url = getYouTubeXMLUrl($url);
    
    $entity_id_array =  getYouTubeXMLUrl($url, true);
    
   $url = $xml_url;
    $xml = simplexml_load_file($url);
    $ns = $xml->getDocNamespaces(true);
    $xml->registerXPathNamespace('a', 'http://www.w3.org/2005/Atom');
    $elements = $xml->xpath('//a:entry');
   foreach ($elements as $content) {
      $yt = $content->children('http://www.youtube.com/xml/schemas/2015');
      $media = $content->children('http://search.yahoo.com/mrss/');
      
      $video = new Video;
      $video->url_chanel = $url;
      $video->title = $content->title;
      $video->description = $media->group->description;
      $video->author = $content->author->name;
      $video->datetime = $content->published;
      $video->save(); 

     }
   }

     public function updateVideo(Request $request, $id)
     { 
        $video= Video::find($id);
        
        $video->title = $request->input('title');
        $video->description = $request->input('description');


        $video->save();
        return response()->json($video);
       /* return response()->json($video);
        $video = Video::findOrFail($id);
        $video->update($request->all());

        return response()->json($video, 200);*/
     }

     public function destroyVideo($id)
     {
       /* $video = Product::find($id);
        $video->delete();

         return response()->json('video removed successfully');*/
         Video::findOrFail($id)->delete();
         return response('Video removido com Sucesso', 200);
     }
     
   }